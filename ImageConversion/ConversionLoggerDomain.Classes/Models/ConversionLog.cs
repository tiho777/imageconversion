﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ConversionLoggerDomain.Classes.Models
{
    public class ConversionLog
    {
        [Key]
        public Guid ImageId { get; set; }
        [StringLength(256)]
        public string Path { get; set; }
        public DateTime Created { get; set; }
    }
}
