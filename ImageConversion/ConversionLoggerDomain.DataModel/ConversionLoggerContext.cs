﻿using ConversionLoggerDomain.Classes.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace ConversionLoggerDomain.DataModel
{
    public class ConversionLoggerContext : DbContext
    {
        public ConversionLoggerContext() { }

        public DbSet<ConversionLog> ConversionLog { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}
