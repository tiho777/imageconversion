namespace ConversionLoggerDomain.DataModel.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ConversionLog",
                c => new
                    {
                        ImageId = c.Guid(nullable: false),
                        Path = c.String(maxLength: 256),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ImageId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ConversionLog");
        }
    }
}
