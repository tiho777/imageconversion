﻿using ImageConversion.Manipulator.Interfaces;

namespace ImageConversion.Manipulator.Components
{
    public class ArgumentParser : IParseArgumentsForImageActions
    {
        public const int ErrorImageIsEmpty = -1;
        public const int ErrorExceededAllowedNumber = -2;

        private readonly IHoldImageUploadSettings settings;

        public ArgumentParser(IHoldImageUploadSettings settings)
        {
            this.settings = settings;
        }

        public int ForAdding(IHoldPostedFile postedImage, int imageCount)
        {
            if (postedImage == null)
            {
                return ErrorImageIsEmpty;
            }
            else if (imageCount > settings.MaxImagesPerRequest)
            {
                
                return ErrorExceededAllowedNumber;
            }

            return imageCount;
        }
    }
}
