﻿using ImageConversion.Manipulator.Components.Graphics;
using ImageConversion.Manipulator.Interfaces;
using ImageConversion.Manipulator.Interfaces.File;
using ImageConversion.Manipulator.Interfaces.Graphics;

using System;
using System.Drawing;

namespace ImageConversion.Manipulator.Components.File
{
    public class ImageFileStorage : IStoreImageFiles
    {
        private const long Quality = 100L;

        private readonly IHoldImageUploadSettings settings;
        private readonly IWriteImageFiles file;
        private readonly IGenerateImages image;

        public ImageFileStorage(IHoldImageUploadSettings settings)
        {
            this.settings = settings;
            this.image = new ImageGenerator();
            this.file = new ImageFileWriter();
        }

        public bool Save(Guid guid, IHoldPostedFile postedImage)
        {
            try
            {
                Image original = image.Generate(postedImage.InputStream);

                if (original == null) return false;

                var resized = image.Generate(original, 1024, 768);
              
                file.Save(resized, Path(settings.ImageFileRoot, guid), Quality);
            }
            catch (Exception ex)
            { }

            return true;
        }

        private string Path(string root, Guid guid)
        {
            return
                string.Format(
                    @"{0}\{1:000}.tiff",
                    root,
                    guid.ToString());
        }
    }
}
