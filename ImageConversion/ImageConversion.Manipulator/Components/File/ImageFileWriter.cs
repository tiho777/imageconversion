﻿using ConversionLoggerDomain.Classes.Models;
using ConversionLoggerDomain.DataModel;
using ImageConversion.Manipulator.Interfaces.File;

using System;
using System.Data.Entity.Validation;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;

namespace ImageConversion.Manipulator.Components.File
{
    internal class ImageFileWriter : IWriteImageFiles
    {
        public bool Save(Image image, string path, long quality)
        {
            try
            {
                Encoder myEncoder = Encoder.Quality;
                var myEncoderParameters = new EncoderParameters(1);
                ImageCodecInfo tiffEncoder = GetEncoder(ImageFormat.Tiff);

                var myEncoderParameter = new EncoderParameter(myEncoder, quality);
                myEncoderParameters.Param[0] = myEncoderParameter;

                Directory.CreateDirectory(Path.GetDirectoryName(path));
                image.Save(path, tiffEncoder, myEncoderParameters);

                string[] filePaths = Directory.GetFiles(@"D:\ImageStore\", "*.tiff");
                ConvertToMultipageTiff(filePaths);
            }
            catch (Exception ex)
            {
                return false;
            }

            return true;
        }

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            
            foreach(var codec in codecs)
            {
                if(codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }

            return null; 
        }

        public static void ConvertToMultipageTiff(string[] fileNames)
        {
            EncoderParameters encoderParams = new EncoderParameters(1);
            ImageCodecInfo tiffCodecInfo = ImageCodecInfo.GetImageEncoders()
                .First(ie => ie.MimeType == "image/tiff");

            string[] tiffPaths = null;

                tiffPaths = new string[1];
                Image tiffImg = null;
            try
            {
                for (int i = 0; i < fileNames.Length; i++)
                {
                    if (i == 0)
                    {
                        tiffPaths[i] = String.Format("{0}\\{1}.tif",
                            Path.GetDirectoryName(fileNames[i]),
                            Path.GetFileNameWithoutExtension(fileNames[i]));

                        // Initialize the first frame of multipage tiff.
                        tiffImg = Image.FromFile(fileNames[i]);
                        encoderParams.Param[0] = new EncoderParameter(Encoder.SaveFlag, (long)EncoderValue.MultiFrame);
                        tiffImg.Save(tiffPaths[i], tiffCodecInfo, encoderParams);

                        try
                        {
                            var conversionLog = new ConversionLog();

                            conversionLog.ImageId = new Guid(Path.GetFileNameWithoutExtension(fileNames[i]));
                            conversionLog.Created = DateTime.Now;
                            conversionLog.Path = tiffPaths[i];

                            using (var conversionLoggerContext = new ConversionLoggerContext())
                            {
                                conversionLoggerContext.ConversionLog.Add(conversionLog);
                                conversionLoggerContext.SaveChanges();
                            }
                        }
                        catch (DbEntityValidationException dbEx)
                        {
                            foreach (var validationErrors in dbEx.EntityValidationErrors)
                            {
                                foreach (var validationError in validationErrors.ValidationErrors)
                                {
                                    Console.WriteLine("Property: {0} Error: {1}",
                                                            validationError.PropertyName,
                                                            validationError.ErrorMessage);

                                }
                            }
                        }
                    }
                    else
                    {
                        // Add additional frames.
                        encoderParams.Param[0] = new EncoderParameter(
                            Encoder.SaveFlag, (long)EncoderValue.FrameDimensionPage);
                        using (Image frame = Image.FromFile(fileNames[i]))
                        {
                            tiffImg.SaveAdd(frame, encoderParams);
                        }
                    }

                    if (i == fileNames.Length - 1)
                    {
                        // When it is the last frame, flush the resources and closing.
                        encoderParams.Param[0] = new EncoderParameter( Encoder.SaveFlag, (long)EncoderValue.Flush);
                        tiffImg.SaveAdd(encoderParams);
                    }
                }
            }
            finally
            {
                if (tiffImg != null)
                {
                    tiffImg.Dispose();
                    tiffImg = null;
                }
            }
        }
    }
}
