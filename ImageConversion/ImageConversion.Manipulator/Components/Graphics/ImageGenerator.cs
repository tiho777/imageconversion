﻿using ImageConversion.Manipulator.Interfaces.Graphics;

using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace ImageConversion.Manipulator.Components.Graphics
{
    internal class ImageGenerator : IGenerateImages
    {
        public Image Generate(Image image, int width, int height)
        {
            double ratio = Math.Min((double)width / image.Width, (double)height / image.Height);
            int scaledWidth = (int)Math.Ceiling(ratio * image.Width);
            int scaledHeight = (int)Math.Ceiling(ratio * image.Height);

            var bitmap = new Bitmap(scaledWidth, scaledHeight, PixelFormat.Format24bppRgb);

            using (var graphics = System.Drawing.Graphics.FromImage(bitmap))
            {
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.DrawImage(image, 0, 0, scaledWidth, scaledHeight);
            }

            return bitmap;
        }

        public Image Generate(Stream stream)
        {
            try
            {
                return Image.FromStream(stream);
            }
            catch (Exception ex)
            { }

            return null;
        }
    }
}
