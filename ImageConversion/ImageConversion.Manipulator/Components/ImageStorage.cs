﻿using ImageConversion.Manipulator.Entities;
using ImageConversion.Manipulator.Interfaces;
using ImageConversion.Manipulator.Interfaces.File;

using System;

namespace ImageConversion.Manipulator.Components
{
    public class ImageStorage : IStoreImages
    {
        private readonly IHoldImageUploadSettings settings;
        private readonly IParseArgumentsForImageActions parse;
        private readonly IStoreImageFiles imageFiles;

        public ImageStorage(IHoldImageUploadSettings settings, IParseArgumentsForImageActions parse, IStoreImageFiles imageFiles)
        {
            this.settings = settings;
            this.parse = parse;
            this.imageFiles = imageFiles;
        }

        public ImageResult Add(PostedImage postedImage, int imageCount)
        {
            int value = parse.ForAdding(postedImage, imageCount);

            if (value > 0)
            {
                imageFiles.Save(Guid.NewGuid(), postedImage);

                return Success();
            }

            return Failure();
        }

        public ImageResult Info()
        {
            return Success();
        }

        private ImageResult Result(bool success)
        {
            return new ImageResult(success);
        }

        private ImageResult Success()
        {
            return Result(true);
        }

        private ImageResult Failure()
        {
            return Result(false);
        }
    }
}
