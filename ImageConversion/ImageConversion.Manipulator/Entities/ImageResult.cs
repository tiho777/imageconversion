﻿using ImageConversion.Manipulator.Interfaces;

namespace ImageConversion.Manipulator.Entities
{
    public class ImageResult : IHoldImageResult
    {
        public ImageResult(bool success)
        {
            this.Success = success;
        }

        public bool Success { get; private set; }
    }
}
