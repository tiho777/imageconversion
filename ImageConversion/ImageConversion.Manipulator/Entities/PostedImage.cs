﻿using ImageConversion.Manipulator.Interfaces;

using System;
using System.IO;

namespace ImageConversion.Manipulator.Entities
{
    public class PostedImage : IHoldPostedFile
    {
        public PostedImage(string fileName, Stream inputStream)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentException("File name is empty");
            }

            if (inputStream == null ||
                inputStream.Length <= 0)
            {
                throw new ArgumentException("Input stream is empty");
            }

            this.FileName = fileName;
            this.InputStream = inputStream;
        }

        public string FileName { get; set; }

        public Stream InputStream { get; set; }
    }
}
