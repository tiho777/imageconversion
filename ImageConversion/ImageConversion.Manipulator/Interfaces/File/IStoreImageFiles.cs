﻿using System;

namespace ImageConversion.Manipulator.Interfaces.File
{
    public interface IStoreImageFiles
    {
        bool Save(Guid fileName, IHoldPostedFile postedImage);
    }
}
