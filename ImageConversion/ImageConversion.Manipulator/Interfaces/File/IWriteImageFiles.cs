﻿using System.Drawing;

namespace ImageConversion.Manipulator.Interfaces.File
{
    public interface IWriteImageFiles
    {
        bool Save(Image image, string path, long quality);
    }
}
