﻿using System.Drawing;
using System.IO;

namespace ImageConversion.Manipulator.Interfaces.Graphics
{
    public interface IGenerateImages
    {
        Image Generate(Image image, int width, int height);
        Image Generate(Stream stream);
    }
}
