﻿namespace ImageConversion.Manipulator.Interfaces
{
    public interface IHoldImageResult
    {
        bool Success { get; }
    }
}
