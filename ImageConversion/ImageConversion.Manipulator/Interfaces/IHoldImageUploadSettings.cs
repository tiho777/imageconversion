﻿namespace ImageConversion.Manipulator.Interfaces
{
    public interface IHoldImageUploadSettings
    {
        int MaxImagesPerRequest { get; }
        string ImageFileRoot { get; }
    }
}
