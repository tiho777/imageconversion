﻿using System.IO;

namespace ImageConversion.Manipulator.Interfaces
{
    public interface IHoldPostedFile
    {
        string FileName { get; }
        Stream InputStream { get; }
    }
}
