﻿namespace ImageConversion.Manipulator.Interfaces
{
    public interface IParseArgumentsForImageActions
    {
        int ForAdding(IHoldPostedFile postedImage, int imageCount);
    }
}
