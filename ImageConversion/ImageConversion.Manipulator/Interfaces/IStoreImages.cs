﻿using ImageConversion.Manipulator.Entities;

namespace ImageConversion.Manipulator.Interfaces
{
    public interface IStoreImages
    {
        ImageResult Info();
        ImageResult Add(PostedImage postedImage, int imageCount);
    }
}
