﻿using Autofac;
using ImageConversion.Manipulator.Components;
using ImageConversion.Manipulator.Components.File;
using ImageConversion.Manipulator.Components.Graphics;
using ImageConversion.Manipulator.Interfaces;
using ImageConversion.Manipulator.Interfaces.File;
using ImageConversion.Manipulator.Interfaces.Graphics;

namespace ImageConversion.Manipulator.Modules
{
    public class ImageConversionModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.Register<IHoldImageUploadSettings>(c => UploadSettingsFactory.GetSettings());
            builder.RegisterType<ImageFileStorage>().As<IStoreImageFiles>();
            builder.RegisterType<ImageFileWriter>().As<IWriteImageFiles>();
            builder.RegisterType<ImageGenerator>().As<IGenerateImages>();
            builder.RegisterType<ArgumentParser>().As<IParseArgumentsForImageActions>();
            builder.RegisterType<ImageStorage>().As<IStoreImages>();
        }
    }
}
