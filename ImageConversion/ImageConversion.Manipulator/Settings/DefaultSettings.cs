﻿using ImageConversion.Manipulator.Interfaces;
using System.Configuration;

namespace ImageConversion.Manipulator.Settings
{
    public class DefaultSettings : IHoldImageUploadSettings
    {
        public int MaxImagesPerRequest
        {
            get
            {
                return 3;
            }
        }

        public string ImageFileRoot
        {
            get
            {
                return GetImageFileRoot("ImageFileRoot");
            }
        }

        private static string GetImageFileRoot(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }
    }
}
