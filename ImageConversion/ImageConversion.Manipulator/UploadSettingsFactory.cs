﻿using ImageConversion.Manipulator.Interfaces;
using ImageConversion.Manipulator.Settings;

using System.Web;

namespace ImageConversion.Manipulator
{
    public static class UploadSettingsFactory
    {
        public static IHoldImageUploadSettings GetSettings()
        {
            return 
                   HttpContext.Current.Request != null ? (IHoldImageUploadSettings)new DefaultSettings() : null;
        }
    }
}
