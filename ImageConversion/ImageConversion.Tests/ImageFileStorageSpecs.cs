﻿using System;
using System.IO;
using ImageConversion.Manipulator.Components.File;
using ImageConversion.Manipulator.Interfaces;
using ImageConversion.Manipulator.Interfaces.File;
using ImageConversion.Manipulator.Interfaces.Graphics;

using Moq;
using Drawing = System.Drawing;

using Machine.Fakes;
using Machine.Specifications;

using MoqIt = Moq.It;
using ThenIt = Machine.Specifications.It;

namespace ImageConversion.Tests
{
    public class when_asked_to_save_non_image_file : ImageFileStorageSpecBase
    {
        private Establish context = () =>
            imageConverter.WhenToldTo(x => x.Generate(MoqIt.IsAny<Stream>())).Return((Drawing.Bitmap)null);

        private Because of = () =>
            result = fileStorage.Save(SomeGuid, postedImage);

        private ThenIt should_not_save_file = () =>
            fileWriter.WasNotToldTo(x => x.Save(MoqIt.IsAny<Drawing.Image>(), MoqIt.IsAny<string>(), quality));

        private ThenIt should_return_error = () =>
            result.ShouldBeFalse();
    }

    [Subject(typeof(ImageFileStorage))]
    public class ImageFileStorageSpecBase : WithSubject<ImageFileStorage>
    {
        public static readonly Guid SomeGuid = new Guid("2ffd0e44-5aa3-4ade-9936-71d21b4ee553");

        protected static IStoreImageFiles fileStorage;
        protected static IWriteImageFiles fileWriter;
        protected static IGenerateImages imageConverter;
        protected static IHoldPostedFile postedImage;
        protected static bool result;
        protected static long quality = 100L;

        private Establish context = () =>
        {
            fileWriter = new Mock<IWriteImageFiles>().Object;
            imageConverter = new Mock<IGenerateImages>().Object;
            postedImage = new Mock<IHoldPostedFile>().Object;
            fileStorage = new Mock<IStoreImageFiles>().Object;
        };
    }

}