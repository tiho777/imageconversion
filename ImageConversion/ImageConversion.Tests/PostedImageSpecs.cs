﻿using System;
using System.IO;

using ImageConversion.Manipulator.Entities;

using Machine.Fakes;
using Machine.Specifications;

using ThenIt = Machine.Specifications.It;

namespace ImageConversion.Tests
{
    public class when_created_with_arguments : PostedImageSpecBase
    {
        ThenIt should_have_file_name_set = () =>
            Subject.FileName.ShouldEqual(SomeFileName);

        ThenIt should_have_input_stream_set = () =>
        {
            Subject.InputStream.ShouldNotBeNull();
            new StreamReader(Subject.InputStream).Read().ShouldEqual(SomeByte);
        };
    }

    public class when_created_without_specifying_file_name : PostedImageSpecBase
    {
        ThenIt should_throw_argument = () =>
            exception.ShouldBeThrownBy(() => new PostedImage(string.Empty, SomeInputStream));
    }

    public class when_created_without_providing_image_content : PostedImageSpecBase
    {
        ThenIt should_throw_argument = () =>
            exception.ShouldBeThrownBy(() => new PostedImage(SomeFileName, null));
    }

    public class when_created_using_empty_image_content : PostedImageSpecBase
    {
        ThenIt should_throw_argument = () =>
            exception.ShouldBeThrownBy(() => new PostedImage(SomeFileName, new MemoryStream(new byte[0])));
    }

    [Subject(typeof(PostedImage))]
    public class PostedImageSpecBase : WithSubject<PostedImage>
    {
        protected const string SomeFileName = "Slika12";
        protected const byte SomeByte = 2;
        protected static Stream SomeInputStream = new MemoryStream(new byte[] { SomeByte });
        protected static Type exception = typeof(ArgumentException);

        Establish context = () => Subject = new PostedImage(SomeFileName, SomeInputStream);
    }
}
