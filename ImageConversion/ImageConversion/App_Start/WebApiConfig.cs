﻿using Autofac;
using Autofac.Integration.WebApi;

using ImageConversion.Manipulator.Modules;

using Microsoft.Owin.Security.OAuth;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web.Http;

namespace ImageConversion
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Formatters.Remove(config.Formatters.XmlFormatter);
            config.Formatters.FormUrlEncodedFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("multipart/form-data"));

            // IoC Autofac resolver
            config.DependencyResolver = SetupAutofac();
        }

        private static AutofacWebApiDependencyResolver SetupAutofac()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<ImageConversionModule>();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            return new AutofacWebApiDependencyResolver(builder.Build());
        }
    }
}
