﻿using ImageConversion.Manipulator.Entities;
using ImageConversion.Manipulator.Interfaces;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace ImageConversion.Controllers
{
    
    public class ImageController : ApiController
    {
        private readonly IStoreImages storeImages;

        public ImageController(IStoreImages storeImages)
        {
            this.storeImages = storeImages;
        }

        [Route("api/Image")]
        [HttpGet]
        public HttpResponseMessage Get()
        {
            return Request.CreateResponse(HttpStatusCode.OK, "Image API default route");
        }
       
        [HttpPost]
        [Route("api/Image/MediaUpload")]
        [ResponseType(typeof(ImageResult))]
        public HttpResponseMessage MediaUpload()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            ImageResult result;
            try
            {
                var files = HttpContext.Current.Request.Files;

            }
            catch (HttpException ex)
            {
                BadRequest(ex.ToString());
            }
            return Request.CreateResponse(
                this.Save(HttpContext.Current.Request.Files, out result) ?
                    HttpStatusCode.OK :
                    HttpStatusCode.BadRequest, result);
        }

        #region helpers
        private bool Save(HttpFileCollection files, out ImageResult result)
        {
            result = null;

            if (files == null || files.Count == 0)
            {
                result = new ImageResult(false);
                return false;
            }

            for (int i = 0; i < files.Count; i++)
            {
                this.Save(files[i], out result, files.Count);
            }

            if (files.Count > 1)
            {
                result = storeImages.Info();
            }

            return result.Success;
        }

        private bool Save(HttpPostedFile file, out ImageResult result, int imageCount)
        {
            if (file == null)
            {
                result = new ImageResult(false);
                return false;
            }

            result = storeImages.Add(new PostedImage(file.FileName, file.InputStream), imageCount);

            return result.Success;
        }
        #endregion
    }
}
