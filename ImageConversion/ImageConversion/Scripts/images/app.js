﻿(function () {
    'use strict';

    // Create the module and define its dependencies.
    var app = angular.module('imagesUploadApp', []);

    // Execute bootstrapping code and any dependencies.
    app.run([function () { } ]);

})();