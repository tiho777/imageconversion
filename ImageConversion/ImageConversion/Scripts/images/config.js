﻿(function () {
    'use strict';

    var app = angular.module('imagesUploadApp');

    var config = {
    };

    app.value('config', config);

    app.config([
       '$locationProvider', function ($locationProvider) {
           $locationProvider.html5Mode(true);
       }
    ]);
})();