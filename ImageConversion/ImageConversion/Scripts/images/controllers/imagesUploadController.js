﻿(function () {
    'use strict';

    var controllerId = 'imagesUploadController';

    angular.module('imagesUploadApp').controller(controllerId, ['imagesUploadSvc', '$timeout', imagesUploadController]);

    function imagesUploadController(imagesUploadSvc, $timeout) {
        var $this = this;

        $this.filesCount = 0;
        $this.data = 'none';
        $this.uploading = false;
        $this.ImagesData = {};
        $this.ImagePlaceholders = [];
        $this.placeholders = {
            0: "../../../Images/placeholder.png",
            1: "../../../Images/placeholder.png",
            2: "../../../Images/placeholder.png"
        };

        (function () {
            var assemblePlaceholders = function () {
                for (var i = 0; i < 3 ; i++) {
                    $this.ImagePlaceholders.push(
                    {
                        "image_src": i < 3 ? $this.placeholders[i] : null,
                        "image_index": i + 1,
                        "has_image": i < 3 ? true : false
                    });
                }
            }
            assemblePlaceholders();
        })();

        $this.selectImages = function () {
            $timeout(function () {
                document.getElementById('selectImagesInput').click()
            })
        };

        $this.sendImages = function () {
            $this.uploading = true;

            var imageFiles = document.getElementById('selectImagesInput').files;
            $this.filesCount = imageFiles.length;
         
            if ($this.filesCount > 0) {
                setTimeout(function () { handleImages(imageFiles); }, 3000);
            }
        };

        var handleImages = function (imageFiles) {
            var data = new FormData();

            for (var image in imageFiles) {
                data.append("uploadedFile", imageFiles[image]);
            }

            try {
                imagesUploadSvc.upload(data).then(function (response) {
                    if (response.data.Success == true) {
                        $this.ImagesData.Success = response.data.Success;
                        $this.filesCount = 0;
                        $this.uploading = false;
                    }
                    else {
                        $this.uploading = false;
                    }

                });
            }
            catch (error) {
                $this.uploading = false;
            }
        };

        $this.showLoadingImage = function () {
            if (!$this.uploading) {
                return false;
            }
            return true;
        };
    }

})();