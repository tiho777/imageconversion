﻿(function () {
    'use strict';

    var serviceId = 'imagesUploadSvc';
    angular.module('imagesUploadApp').factory(serviceId, ['$http', '$q', imagesUploadSvc]);

    function imagesUploadSvc($http, $q) {
        var upload = function (formData) {
            return $http.post('http://localhost:19500/api/image/MediaUpload', formData, {
                transformRequest: angular.identity,
                headers: { 'Content-Type': undefined },
                timeout: 180000
            }).then(function onSuccess(response) {
                // Handle success
                var data = response.data;
                var status = response.status;
                var statusText = response.statusText;
                var headers = response.headers;
                var config = response.config;
                return response;
            }).catch(function onError(response) {
                // Handle error
                var data = response.data;
                var status = response.status;
                var statusText = response.statusText;
                var headers = response.headers;
                var config = response.config;
            });
        };

        var service = {
            upload: upload
        };

        return service;
    };

})();